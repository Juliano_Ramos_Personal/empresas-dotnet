/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:09 ******/
DROP INDEX [IdxUser] ON [dbo].[User]
GO
/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:09 ******/
DROP INDEX [IdxUser] ON [dbo].[EnterpriseType]
GO
/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:09 ******/
DROP INDEX [IdxUser] ON [dbo].[Enterprise]
GO
/****** Object:  Table [dbo].[User]    Script Date: 31/05/2019 22:24:09 ******/
DROP TABLE [dbo].[User]
GO
/****** Object:  Table [dbo].[EnterpriseType]    Script Date: 31/05/2019 22:24:09 ******/
DROP TABLE [dbo].[EnterpriseType]
GO
/****** Object:  Table [dbo].[Enterprise]    Script Date: 31/05/2019 22:24:09 ******/
DROP TABLE [dbo].[Enterprise]
GO
/****** Object:  Table [dbo].[Enterprise]    Script Date: 31/05/2019 22:24:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Enterprise](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](300) NOT NULL,
	[Phone] [varchar](20) NOT NULL,
	[Name] [varchar](300) NOT NULL,
	[EnterpriseType_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_Enterprise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EnterpriseType]    Script Date: 31/05/2019 22:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnterpriseType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EnterpriseType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 31/05/2019 22:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](300) NOT NULL,
	[Password] [varchar](300) NOT NULL,
	[Client] [varchar](300) NULL,
	[AccessToken] [varchar](200) NULL,
	[ExpirationTime] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Enterprise] ON 

INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (1, N'aQm@aQm.com', N'214590011', N'aQm', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (2, N'Enterprise2@enter.com', N'321232212', N'Enterprise2', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (3, N'enterprise1@enterprise1.com', N'2258748', N'Enterprise1', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (4, N'enterprise2@enterprise2.com', N'2277940', N'Enterprise2', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (5, N'enterprise3@enterprise3.com', N'2245392', N'Enterprise3', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (6, N'enterprise4@enterprise4.com', N'2220972', N'Enterprise4', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (7, N'enterprise5@enterprise5.com', N'2201951', N'Enterprise5', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (8, N'enterprise6@enterprise6.com', N'2242789', N'Enterprise6', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (9, N'enterprise7@enterprise7.com', N'2218965', N'Enterprise7', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (10, N'enterprise8@enterprise8.com', N'2249165', N'Enterprise8', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (11, N'enterprise9@enterprise9.com', N'2261772', N'Enterprise9', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (12, N'enterprise10@enterprise10.com', N'2243360', N'Enterprise10', 1)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (13, N'enterprise1@enterprise1.com', N'2278999', N'Enterprise1', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (14, N'enterprise2@enterprise2.com', N'2278451', N'Enterprise2', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (15, N'enterprise3@enterprise3.com', N'2261293', N'Enterprise3', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (16, N'enterprise4@enterprise4.com', N'2237987', N'Enterprise4', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (17, N'enterprise5@enterprise5.com', N'2263664', N'Enterprise5', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (18, N'enterprise6@enterprise6.com', N'2225476', N'Enterprise6', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (19, N'enterprise7@enterprise7.com', N'2248613', N'Enterprise7', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (20, N'enterprise8@enterprise8.com', N'2293480', N'Enterprise8', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (21, N'enterprise9@enterprise9.com', N'2288428', N'Enterprise9', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (22, N'enterprise10@enterprise10.com', N'2274492', N'Enterprise10', 2)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (23, N'enterprise1@enterprise1.com', N'2209558', N'Enterprise1', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (24, N'enterprise2@enterprise2.com', N'2262428', N'Enterprise2', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (25, N'enterprise3@enterprise3.com', N'2242575', N'Enterprise3', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (26, N'enterprise4@enterprise4.com', N'2214406', N'Enterprise4', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (27, N'enterprise5@enterprise5.com', N'2252761', N'Enterprise5', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (28, N'enterprise6@enterprise6.com', N'2214722', N'Enterprise6', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (29, N'enterprise7@enterprise7.com', N'2289018', N'Enterprise7', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (30, N'enterprise8@enterprise8.com', N'2233344', N'Enterprise8', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (31, N'enterprise9@enterprise9.com', N'2210064', N'Enterprise9', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (32, N'enterprise10@enterprise10.com', N'2228250', N'Enterprise10', 3)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (33, N'enterprise1@enterprise1.com', N'2294554', N'Enterprise1', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (34, N'enterprise2@enterprise2.com', N'2291326', N'Enterprise2', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (35, N'enterprise3@enterprise3.com', N'2213177', N'Enterprise3', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (36, N'enterprise4@enterprise4.com', N'2239910', N'Enterprise4', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (37, N'enterprise5@enterprise5.com', N'2260191', N'Enterprise5', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (38, N'enterprise6@enterprise6.com', N'2218204', N'Enterprise6', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (39, N'enterprise7@enterprise7.com', N'2258847', N'Enterprise7', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (40, N'enterprise8@enterprise8.com', N'2285890', N'Enterprise8', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (41, N'enterprise9@enterprise9.com', N'2285673', N'Enterprise9', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (42, N'enterprise10@enterprise10.com', N'2276459', N'Enterprise10', 4)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (43, N'enterprise1@enterprise1.com', N'2264065', N'Enterprise1', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (44, N'enterprise2@enterprise2.com', N'2230646', N'Enterprise2', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (45, N'enterprise3@enterprise3.com', N'2206937', N'Enterprise3', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (46, N'enterprise4@enterprise4.com', N'2222338', N'Enterprise4', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (47, N'enterprise5@enterprise5.com', N'2208354', N'Enterprise5', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (48, N'enterprise6@enterprise6.com', N'2279715', N'Enterprise6', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (49, N'enterprise7@enterprise7.com', N'2286604', N'Enterprise7', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (50, N'enterprise8@enterprise8.com', N'2299643', N'Enterprise8', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (51, N'enterprise9@enterprise9.com', N'2292646', N'Enterprise9', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (52, N'enterprise10@enterprise10.com', N'2211167', N'Enterprise10', 5)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (53, N'enterprise1@enterprise1.com', N'2219612', N'Enterprise1', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (54, N'enterprise2@enterprise2.com', N'2234017', N'Enterprise2', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (55, N'enterprise3@enterprise3.com', N'2202087', N'Enterprise3', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (56, N'enterprise4@enterprise4.com', N'2210174', N'Enterprise4', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (57, N'enterprise5@enterprise5.com', N'2228529', N'Enterprise5', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (58, N'enterprise6@enterprise6.com', N'2268944', N'Enterprise6', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (59, N'enterprise7@enterprise7.com', N'2207988', N'Enterprise7', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (60, N'enterprise8@enterprise8.com', N'2203609', N'Enterprise8', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (61, N'enterprise9@enterprise9.com', N'2240834', N'Enterprise9', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (62, N'enterprise10@enterprise10.com', N'2259853', N'Enterprise10', 6)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (63, N'enterprise1@enterprise1.com', N'2226395', N'Enterprise1', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (64, N'enterprise2@enterprise2.com', N'2258245', N'Enterprise2', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (65, N'enterprise3@enterprise3.com', N'2211725', N'Enterprise3', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (66, N'enterprise4@enterprise4.com', N'2206145', N'Enterprise4', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (67, N'enterprise5@enterprise5.com', N'2221430', N'Enterprise5', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (68, N'enterprise6@enterprise6.com', N'2260297', N'Enterprise6', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (69, N'enterprise7@enterprise7.com', N'2237898', N'Enterprise7', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (70, N'enterprise8@enterprise8.com', N'2294007', N'Enterprise8', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (71, N'enterprise9@enterprise9.com', N'2250420', N'Enterprise9', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (72, N'enterprise10@enterprise10.com', N'2218084', N'Enterprise10', 7)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (73, N'enterprise1@enterprise1.com', N'2239253', N'Enterprise1', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (74, N'enterprise2@enterprise2.com', N'2235346', N'Enterprise2', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (75, N'enterprise3@enterprise3.com', N'2280431', N'Enterprise3', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (76, N'enterprise4@enterprise4.com', N'2284594', N'Enterprise4', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (77, N'enterprise5@enterprise5.com', N'2283051', N'Enterprise5', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (78, N'enterprise6@enterprise6.com', N'2251536', N'Enterprise6', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (79, N'enterprise7@enterprise7.com', N'2228115', N'Enterprise7', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (80, N'enterprise8@enterprise8.com', N'2292862', N'Enterprise8', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (81, N'enterprise9@enterprise9.com', N'2278676', N'Enterprise9', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (82, N'enterprise10@enterprise10.com', N'2245669', N'Enterprise10', 8)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (83, N'enterprise1@enterprise1.com', N'2200742', N'Enterprise1', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (84, N'enterprise2@enterprise2.com', N'2261999', N'Enterprise2', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (85, N'enterprise3@enterprise3.com', N'2213321', N'Enterprise3', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (86, N'enterprise4@enterprise4.com', N'2271520', N'Enterprise4', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (87, N'enterprise5@enterprise5.com', N'2281262', N'Enterprise5', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (88, N'enterprise6@enterprise6.com', N'2233161', N'Enterprise6', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (89, N'enterprise7@enterprise7.com', N'2261863', N'Enterprise7', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (90, N'enterprise8@enterprise8.com', N'2275626', N'Enterprise8', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (91, N'enterprise9@enterprise9.com', N'2284005', N'Enterprise9', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (92, N'enterprise10@enterprise10.com', N'2263871', N'Enterprise10', 9)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (93, N'enterprise1@enterprise1.com', N'2282840', N'Enterprise1', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (94, N'enterprise2@enterprise2.com', N'2227861', N'Enterprise2', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (95, N'enterprise3@enterprise3.com', N'2217680', N'Enterprise3', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (96, N'enterprise4@enterprise4.com', N'2298443', N'Enterprise4', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (97, N'enterprise5@enterprise5.com', N'2222688', N'Enterprise5', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (98, N'enterprise6@enterprise6.com', N'2278444', N'Enterprise6', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (99, N'enterprise7@enterprise7.com', N'2291545', N'Enterprise7', 10)
GO
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (100, N'enterprise8@enterprise8.com', N'2273416', N'Enterprise8', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (101, N'enterprise9@enterprise9.com', N'2273531', N'Enterprise9', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (102, N'enterprise10@enterprise10.com', N'2220680', N'Enterprise10', 10)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (103, N'enterprise1@enterprise1.com', N'2214442', N'Enterprise1', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (104, N'enterprise2@enterprise2.com', N'2265221', N'Enterprise2', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (105, N'enterprise3@enterprise3.com', N'2221846', N'Enterprise3', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (106, N'enterprise4@enterprise4.com', N'2247187', N'Enterprise4', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (107, N'enterprise5@enterprise5.com', N'2244538', N'Enterprise5', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (108, N'enterprise6@enterprise6.com', N'2256889', N'Enterprise6', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (109, N'enterprise7@enterprise7.com', N'2226210', N'Enterprise7', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (110, N'enterprise8@enterprise8.com', N'2290117', N'Enterprise8', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (111, N'enterprise9@enterprise9.com', N'2299748', N'Enterprise9', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (112, N'enterprise10@enterprise10.com', N'2249618', N'Enterprise10', 11)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (113, N'enterprise1@enterprise1.com', N'2213452', N'Enterprise1', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (114, N'enterprise2@enterprise2.com', N'2287750', N'Enterprise2', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (115, N'enterprise3@enterprise3.com', N'2297557', N'Enterprise3', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (116, N'enterprise4@enterprise4.com', N'2267789', N'Enterprise4', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (117, N'enterprise5@enterprise5.com', N'2231735', N'Enterprise5', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (118, N'enterprise6@enterprise6.com', N'2237019', N'Enterprise6', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (119, N'enterprise7@enterprise7.com', N'2217548', N'Enterprise7', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (120, N'enterprise8@enterprise8.com', N'2212828', N'Enterprise8', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (121, N'enterprise9@enterprise9.com', N'2297507', N'Enterprise9', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (122, N'enterprise10@enterprise10.com', N'2260893', N'Enterprise10', 12)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (123, N'enterprise1@enterprise1.com', N'2271058', N'Enterprise1', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (124, N'enterprise2@enterprise2.com', N'2234895', N'Enterprise2', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (125, N'enterprise3@enterprise3.com', N'2282626', N'Enterprise3', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (126, N'enterprise4@enterprise4.com', N'2259173', N'Enterprise4', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (127, N'enterprise5@enterprise5.com', N'2207868', N'Enterprise5', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (128, N'enterprise6@enterprise6.com', N'2284015', N'Enterprise6', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (129, N'enterprise7@enterprise7.com', N'2290291', N'Enterprise7', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (130, N'enterprise8@enterprise8.com', N'2254611', N'Enterprise8', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (131, N'enterprise9@enterprise9.com', N'2234530', N'Enterprise9', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (132, N'enterprise10@enterprise10.com', N'2277230', N'Enterprise10', 13)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (133, N'enterprise1@enterprise1.com', N'2232614', N'Enterprise1', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (134, N'enterprise2@enterprise2.com', N'2227323', N'Enterprise2', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (135, N'enterprise3@enterprise3.com', N'2291954', N'Enterprise3', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (136, N'enterprise4@enterprise4.com', N'2230620', N'Enterprise4', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (137, N'enterprise5@enterprise5.com', N'2203852', N'Enterprise5', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (138, N'enterprise6@enterprise6.com', N'2208145', N'Enterprise6', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (139, N'enterprise7@enterprise7.com', N'2212777', N'Enterprise7', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (140, N'enterprise8@enterprise8.com', N'2238758', N'Enterprise8', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (141, N'enterprise9@enterprise9.com', N'2299606', N'Enterprise9', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (142, N'enterprise10@enterprise10.com', N'2296997', N'Enterprise10', 14)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (143, N'enterprise1@enterprise1.com', N'2279575', N'Enterprise1', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (144, N'enterprise2@enterprise2.com', N'2244370', N'Enterprise2', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (145, N'enterprise3@enterprise3.com', N'2248503', N'Enterprise3', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (146, N'enterprise4@enterprise4.com', N'2207011', N'Enterprise4', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (147, N'enterprise5@enterprise5.com', N'2223043', N'Enterprise5', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (148, N'enterprise6@enterprise6.com', N'2267713', N'Enterprise6', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (149, N'enterprise7@enterprise7.com', N'2202929', N'Enterprise7', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (150, N'enterprise8@enterprise8.com', N'2222514', N'Enterprise8', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (151, N'enterprise9@enterprise9.com', N'2239332', N'Enterprise9', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (152, N'enterprise10@enterprise10.com', N'2258292', N'Enterprise10', 15)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (153, N'enterprise1@enterprise1.com', N'2233520', N'Enterprise1', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (154, N'enterprise2@enterprise2.com', N'2251340', N'Enterprise2', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (155, N'enterprise3@enterprise3.com', N'2248533', N'Enterprise3', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (156, N'enterprise4@enterprise4.com', N'2285262', N'Enterprise4', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (157, N'enterprise5@enterprise5.com', N'2222246', N'Enterprise5', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (158, N'enterprise6@enterprise6.com', N'2252170', N'Enterprise6', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (159, N'enterprise7@enterprise7.com', N'2255219', N'Enterprise7', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (160, N'enterprise8@enterprise8.com', N'2235297', N'Enterprise8', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (161, N'enterprise9@enterprise9.com', N'2211943', N'Enterprise9', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (162, N'enterprise10@enterprise10.com', N'2233147', N'Enterprise10', 16)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (163, N'enterprise1@enterprise1.com', N'2239223', N'Enterprise1', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (164, N'enterprise2@enterprise2.com', N'2244222', N'Enterprise2', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (165, N'enterprise3@enterprise3.com', N'2228211', N'Enterprise3', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (166, N'enterprise4@enterprise4.com', N'2295141', N'Enterprise4', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (167, N'enterprise5@enterprise5.com', N'2289627', N'Enterprise5', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (168, N'enterprise6@enterprise6.com', N'2258753', N'Enterprise6', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (169, N'enterprise7@enterprise7.com', N'2268420', N'Enterprise7', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (170, N'enterprise8@enterprise8.com', N'2239561', N'Enterprise8', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (171, N'enterprise9@enterprise9.com', N'2236561', N'Enterprise9', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (172, N'enterprise10@enterprise10.com', N'2296214', N'Enterprise10', 17)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (173, N'enterprise1@enterprise1.com', N'2296866', N'Enterprise1', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (174, N'enterprise2@enterprise2.com', N'2251413', N'Enterprise2', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (175, N'enterprise3@enterprise3.com', N'2285847', N'Enterprise3', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (176, N'enterprise4@enterprise4.com', N'2273134', N'Enterprise4', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (177, N'enterprise5@enterprise5.com', N'2253942', N'Enterprise5', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (178, N'enterprise6@enterprise6.com', N'2299312', N'Enterprise6', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (179, N'enterprise7@enterprise7.com', N'2259016', N'Enterprise7', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (180, N'enterprise8@enterprise8.com', N'2248239', N'Enterprise8', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (181, N'enterprise9@enterprise9.com', N'2208754', N'Enterprise9', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (182, N'enterprise10@enterprise10.com', N'2212779', N'Enterprise10', 18)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (183, N'enterprise1@enterprise1.com', N'2253710', N'Enterprise1', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (184, N'enterprise2@enterprise2.com', N'2240400', N'Enterprise2', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (185, N'enterprise3@enterprise3.com', N'2287903', N'Enterprise3', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (186, N'enterprise4@enterprise4.com', N'2248861', N'Enterprise4', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (187, N'enterprise5@enterprise5.com', N'2216333', N'Enterprise5', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (188, N'enterprise6@enterprise6.com', N'2253898', N'Enterprise6', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (189, N'enterprise7@enterprise7.com', N'2251414', N'Enterprise7', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (190, N'enterprise8@enterprise8.com', N'2279316', N'Enterprise8', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (191, N'enterprise9@enterprise9.com', N'2280998', N'Enterprise9', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (192, N'enterprise10@enterprise10.com', N'2224305', N'Enterprise10', 19)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (193, N'enterprise1@enterprise1.com', N'2225550', N'Enterprise1', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (194, N'enterprise2@enterprise2.com', N'2250103', N'Enterprise2', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (195, N'enterprise3@enterprise3.com', N'2299440', N'Enterprise3', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (196, N'enterprise4@enterprise4.com', N'2239082', N'Enterprise4', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (197, N'enterprise5@enterprise5.com', N'2230398', N'Enterprise5', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (198, N'enterprise6@enterprise6.com', N'2207977', N'Enterprise6', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (199, N'enterprise7@enterprise7.com', N'2288782', N'Enterprise7', 20)
GO
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (200, N'enterprise8@enterprise8.com', N'2225985', N'Enterprise8', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (201, N'enterprise9@enterprise9.com', N'2290008', N'Enterprise9', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (202, N'enterprise10@enterprise10.com', N'2236914', N'Enterprise10', 20)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (203, N'enterprise1@enterprise1.com', N'2271917', N'Enterprise1', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (204, N'enterprise2@enterprise2.com', N'2235992', N'Enterprise2', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (205, N'enterprise3@enterprise3.com', N'2258577', N'Enterprise3', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (206, N'enterprise4@enterprise4.com', N'2215742', N'Enterprise4', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (207, N'enterprise5@enterprise5.com', N'2234050', N'Enterprise5', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (208, N'enterprise6@enterprise6.com', N'2254731', N'Enterprise6', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (209, N'enterprise7@enterprise7.com', N'2238118', N'Enterprise7', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (210, N'enterprise8@enterprise8.com', N'2248023', N'Enterprise8', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (211, N'enterprise9@enterprise9.com', N'2203944', N'Enterprise9', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (212, N'enterprise10@enterprise10.com', N'2295558', N'Enterprise10', 21)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (213, N'enterprise1@enterprise1.com', N'2265778', N'Enterprise1', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (214, N'enterprise2@enterprise2.com', N'2279604', N'Enterprise2', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (215, N'enterprise3@enterprise3.com', N'2299167', N'Enterprise3', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (216, N'enterprise4@enterprise4.com', N'2271788', N'Enterprise4', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (217, N'enterprise5@enterprise5.com', N'2252060', N'Enterprise5', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (218, N'enterprise6@enterprise6.com', N'2271447', N'Enterprise6', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (219, N'enterprise7@enterprise7.com', N'2228943', N'Enterprise7', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (220, N'enterprise8@enterprise8.com', N'2251974', N'Enterprise8', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (221, N'enterprise9@enterprise9.com', N'2251432', N'Enterprise9', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (222, N'enterprise10@enterprise10.com', N'2208353', N'Enterprise10', 22)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (223, N'enterprise1@enterprise1.com', N'2279963', N'Enterprise1', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (224, N'enterprise2@enterprise2.com', N'2261728', N'Enterprise2', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (225, N'enterprise3@enterprise3.com', N'2231187', N'Enterprise3', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (226, N'enterprise4@enterprise4.com', N'2284880', N'Enterprise4', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (227, N'enterprise5@enterprise5.com', N'2243287', N'Enterprise5', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (228, N'enterprise6@enterprise6.com', N'2219387', N'Enterprise6', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (229, N'enterprise7@enterprise7.com', N'2224597', N'Enterprise7', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (230, N'enterprise8@enterprise8.com', N'2219388', N'Enterprise8', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (231, N'enterprise9@enterprise9.com', N'2215359', N'Enterprise9', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (232, N'enterprise10@enterprise10.com', N'2218589', N'Enterprise10', 23)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (233, N'enterprise1@enterprise1.com', N'2283154', N'Enterprise1', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (234, N'enterprise2@enterprise2.com', N'2205655', N'Enterprise2', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (235, N'enterprise3@enterprise3.com', N'2230855', N'Enterprise3', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (236, N'enterprise4@enterprise4.com', N'2244464', N'Enterprise4', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (237, N'enterprise5@enterprise5.com', N'2213486', N'Enterprise5', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (238, N'enterprise6@enterprise6.com', N'2290343', N'Enterprise6', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (239, N'enterprise7@enterprise7.com', N'2254446', N'Enterprise7', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (240, N'enterprise8@enterprise8.com', N'2244785', N'Enterprise8', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (241, N'enterprise9@enterprise9.com', N'2287435', N'Enterprise9', 24)
INSERT [dbo].[Enterprise] ([Id], [Email], [Phone], [Name], [EnterpriseType_Id]) VALUES (242, N'enterprise10@enterprise10.com', N'2234226', N'Enterprise10', 24)
SET IDENTITY_INSERT [dbo].[Enterprise] OFF
SET IDENTITY_INSERT [dbo].[EnterpriseType] ON 

INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (1, N'Agro')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (2, N'Aviation')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (3, N'Biotech')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (4, N'Eco')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (5, N'Ecommerce')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (6, N'Education')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (7, N'Fashion')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (8, N'Fintech')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (9, N'Food')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (10, N'Games')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (11, N'Health')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (12, N'IOT')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (13, N'Logistics')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (14, N'Media')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (15, N'Mining')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (16, N'Products')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (17, N'Real Estate')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (18, N'Service')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (19, N'Smart City')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (20, N'Social')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (21, N'Software')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (22, N'Technology')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (23, N'Tourism')
INSERT [dbo].[EnterpriseType] ([Id], [Name]) VALUES (24, N'Transport')
SET IDENTITY_INSERT [dbo].[EnterpriseType] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Email], [Password], [Client], [AccessToken], [ExpirationTime]) VALUES (1, N'testeapple@ioasys.com.br', N'ed2b1f468c5f915f3f1cf75d7068baae', N'Role.Enterprise', N'9abe9e3917535643688c61c10c00cab4', CAST(N'2019-05-31T22:41:08.473' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:11 ******/
CREATE NONCLUSTERED INDEX [IdxUser] ON [dbo].[Enterprise]
(
	[Name] ASC
)
INCLUDE ( 	[EnterpriseType_Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:11 ******/
CREATE NONCLUSTERED INDEX [IdxUser] ON [dbo].[EnterpriseType]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IdxUser]    Script Date: 31/05/2019 22:24:11 ******/
CREATE NONCLUSTERED INDEX [IdxUser] ON [dbo].[User]
(
	[Email] ASC
)
INCLUDE ( 	[Password],
	[Client],
	[AccessToken]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 60) ON [PRIMARY]
GO
