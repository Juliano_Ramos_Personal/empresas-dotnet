﻿using Microsoft.AspNetCore.Authentication;
using System;

namespace IoaSysEnterprise.WebApi.Auth
{
    public static class AuthBuilderExtensions
    {
        public static AuthenticationBuilder Add(this AuthenticationBuilder builder, Action<AuthOptions> configureOptions)
        {
            return builder.AddScheme<AuthOptions, AuthorizeHandler>(AuthOptions.DefaultScheme, configureOptions);
        }
    }
}
