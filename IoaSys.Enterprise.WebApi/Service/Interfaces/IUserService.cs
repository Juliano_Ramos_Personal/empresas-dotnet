﻿using IoaSysEnterprise.WebApi.Data.Entities;

namespace IoaSysEnterprise.WebApi.Services.Interfaces
{
    public interface IUserService
    {
        User Auth(string email, string password);
        User ValidateAccess(string accessToken, string client, string uid);
    }
}
