﻿using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IoaSysEnterprise.WebApi.Controllers
{
    [Authorize]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class EnterprisesController : Controller
    {
        private IEnterpriseService _service;

        public EnterprisesController(IEnterpriseService service)
        {
            _service = service;
        }

        [HttpGet("list/{id}")]
        public IActionResult List(int id)
        {
            var data = _service.GetByTypeId(id);
            return new ObjectResult(data);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var data = _service.GetById(id);
            return new ObjectResult(data);
        }

        [HttpGet]
        public IActionResult GetByTypeAndName(int enterprise_types, string name)
        {
            var data = _service.GetByTypeIdAndName(enterprise_types, name);
            return new ObjectResult(data);
        }

    }
}
