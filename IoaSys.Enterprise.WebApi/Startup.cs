﻿using IoaSysEnterprise.WebApi.Auth;
using IoaSysEnterprise.WebApi.Config;
using IoaSysEnterprise.WebApi.Data;
using IoaSysEnterprise.WebApi.Service;
using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IoaSysEnterprise.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<DatabaseContext>(options => options.UseLazyLoadingProxies().UseSqlServer(Configuration.GetConnectionString("DatabaseContext")));

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IEnterpriseService, EnterpriseService>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = AuthOptions.DefaultScheme;
                options.DefaultChallengeScheme = AuthOptions.DefaultScheme;
            })
           .Add(options =>{});

            services.Configure<ApiConfig>(options => Configuration.GetSection("ApiConfig").Bind(options));

            services.AddApiVersioning(options=> {
                options.AssumeDefaultVersionWhenUnspecified = false;
                options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
            });

            services.AddMvc();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
