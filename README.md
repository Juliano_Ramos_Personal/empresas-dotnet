# README #

O projeto foi criado baseado na estrutura enviada do Postman.
Como não encontrei na collection um exemplo de Listagem de Empresas pelo Tipo de Empresa, criei o seeguinte endpoint:

{{dev_host}}/api/{{api_version}}/enterprises/list/1

O script de criação do Banco de Dados está no arquivo: **DumpSqlServer.sql**


### Dados para Teste ###

* Servidor: http://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234
