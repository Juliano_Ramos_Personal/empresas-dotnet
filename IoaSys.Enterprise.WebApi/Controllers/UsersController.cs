﻿using IoaSysEnterprise.WebApi.Model;
using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IoaSysEnterprise.WebApi.Controllers
{
    [AllowAnonymous]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : Controller
    {

        private IUserService _service;

        public UsersController(IUserService service)
        {
            _service = service;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("auth/sign_in")]
        public object Post([FromBody]LoginModel login)
        {
            try
            {
                var data = _service.Auth(login.Email, login.Password);

                if (data != null)
                {
                    Request.HttpContext.Response.Headers.Add("access-token", data.AccessToken);
                    Request.HttpContext.Response.Headers.Add("client", data.Client);
                    Request.HttpContext.Response.Headers.Add("uid", data.Email);

                    return new
                    {
                        success = true,
                        message = "Authentication OK"
                    };
                }
                else
                {
                    return new
                    {
                        success = false,
                        message = "Invalid credentials"
                    };
                }
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex);
            }
        }


    }
}
