﻿using IoaSysEnterprise.WebApi.Config;
using IoaSysEnterprise.WebApi.Data;
using IoaSysEnterprise.WebApi.Data.Entities;
using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;

namespace IoaSysEnterprise.WebApi.Service
{
    public class EnterpriseService : IEnterpriseService
    {
        private DatabaseContext _context;
        private IOptions<ApiConfig> _config;

        public EnterpriseService(DatabaseContext context, IOptions<ApiConfig> config)
        {
            _context = context;
            _config = config;
        }

        public Enterprise GetById(int id)
        {
            return _context.Enterprise.Where(x => x.Id == id).FirstOrDefault();
        }

        public Enterprise GetByName(string name)
        {
            return _context.Enterprise.Where(x => x.Name == name).FirstOrDefault();
        }

        public List<Enterprise> GetByTypeId(int type)
        {
            return _context.Enterprise.Where(x => x.EnterpriseType_Id == type).ToList();
        }

        public Enterprise GetByTypeIdAndName(int type, string name)
        {
            return _context.Enterprise.Where(x => x.EnterpriseType_Id == type && x.Name == name).FirstOrDefault();
        }
        
    }
}
