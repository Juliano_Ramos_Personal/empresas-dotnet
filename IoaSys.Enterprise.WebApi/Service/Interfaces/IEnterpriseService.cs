﻿using IoaSysEnterprise.WebApi.Data.Entities;
using System.Collections.Generic;

namespace IoaSysEnterprise.WebApi.Services.Interfaces
{
    public interface IEnterpriseService
    {
        Enterprise GetById(int id);
        Enterprise GetByName(string name);
        List<Enterprise> GetByTypeId(int type);
        Enterprise GetByTypeIdAndName(int type, string name);
    }
}
