﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoaSysEnterprise.WebApi.Data.Entities
{
    public class User
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Client { get; set; }
        public string AccessToken { get; set; }
        public DateTime? ExpirationTime { get; set; }
    }
}
