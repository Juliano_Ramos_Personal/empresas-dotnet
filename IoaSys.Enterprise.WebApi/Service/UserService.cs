﻿using IoaSysEnterprise.WebApi.Auth;
using IoaSysEnterprise.WebApi.Config;
using IoaSysEnterprise.WebApi.Data;
using IoaSysEnterprise.WebApi.Data.Entities;
using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace IoaSysEnterprise.WebApi.Service
{
    public class UserService : IUserService
    {
        private DatabaseContext _context;
        private IOptions<ApiConfig> _config;

        public UserService(DatabaseContext context, IOptions<ApiConfig> config)
        {
            _context = context;
            _config = config;
        }

        public User Auth(string email, string password)
        {
            try
            {
                User user = _context.Users.Where(x => x.Email == email && x.Password == password.ToMd5Hash()).FirstOrDefault();

                if (user != null)
                {
                    var expirationTime = DateTime.Now.AddMinutes(_config.Value.ExpirationTime);
                    user.AccessToken = $"{user.Client}{user.Id}{user.Email}{expirationTime.ToString()}".ToMd5Hash();
                    user.ExpirationTime = expirationTime;
                    _context.SaveChanges();
                    return user;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
       

        public User ValidateAccess(string accessToken, string client, string uid)
        {
            try
            {
                return _context.Users.Where(x => x.Email == uid && x.AccessToken == accessToken && x.Client == client).FirstOrDefault();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
