﻿using IoaSysEnterprise.WebApi.Data.Entities;
using IoaSysEnterprise.WebApi.Services.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace IoaSysEnterprise.WebApi.Auth
{
    public class AuthorizeHandler : AuthenticationHandler<AuthOptions>
    {
        private readonly IUserService _userService;

        public AuthorizeHandler(IOptionsMonitor<AuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IUserService userService)
            : base(options, logger, encoder, clock)
        {
            _userService = userService;
        }
        
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.TryGetValue("access-token", out var accessToken) || !Request.Headers.TryGetValue("client", out var client) || !Request.Headers.TryGetValue("uid", out var uid))
                return Task.FromResult(AuthenticateResult.Fail("Error Loading Header Information"));
            
            User user = _userService.ValidateAccess(accessToken, client, uid);

            if (user == null)
                return Task.FromResult(AuthenticateResult.Fail("Auth Token Invalid"));
            else if(user.ExpirationTime < DateTime.Now)
                return Task.FromResult(AuthenticateResult.Fail("Auth Token Expired"));

            var identities = new List<ClaimsIdentity> { new ClaimsIdentity(user.Id.ToString()) };
            var authTicket = new AuthenticationTicket(new ClaimsPrincipal(identities), Options.Scheme);

            return Task.FromResult(AuthenticateResult.Success(authTicket));
        }
    }
}
