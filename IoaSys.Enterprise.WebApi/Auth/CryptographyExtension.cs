﻿using System.Security.Cryptography;
using System.Text;

namespace IoaSysEnterprise.WebApi.Auth
{
    public static class CryptographyExtension
    {
        public static string ToMd5Hash(this string value)
        {
            MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));
            
            return sBuilder.ToString();
        }
    }
}
