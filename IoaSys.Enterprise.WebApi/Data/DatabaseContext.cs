﻿using Microsoft.EntityFrameworkCore;
using IoaSysEnterprise.WebApi.Data.Entities;

namespace IoaSysEnterprise.WebApi.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(
           DbContextOptions<DatabaseContext> options) : base(options)
        { }

        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserModelBuilder(modelBuilder);
            EnterpriseTypeModelBuilder(modelBuilder);
            EnterpriseModelBuilder(modelBuilder);
        }

        private void EnterpriseModelBuilder(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Enterprise>();
            model.ToTable("Enterprise");
            model.HasKey(x => x.Id);
            model.HasOne(x => x.EnterpriseType).WithMany(x => x.Enterprise).HasForeignKey(x => x.EnterpriseType_Id).IsRequired();
            model.Property(x => x.Email).HasMaxLength(300);
            model.Property(x => x.Phone).HasMaxLength(300);
            model.Property(x => x.Name).HasMaxLength(300).IsRequired();
        }

        private void EnterpriseTypeModelBuilder(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<EnterpriseType>();
            model.ToTable("EnterpriseType");
            model.HasKey(x => x.Id);
            model.Property(x => x.Name).HasMaxLength(300).IsRequired();
        }

        private void UserModelBuilder(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<User>();
            model.ToTable("User");
            model.HasKey(x => x.Id);
            model.Property(x => x.Email).HasMaxLength(300).IsRequired();
            model.Property(x => x.Password).HasMaxLength(300).IsRequired();
            model.Property(x => x.Client).HasMaxLength(300);
            model.Property(x => x.AccessToken).HasMaxLength(300);
            model.Property(x => x.ExpirationTime);
        }
    }
}
