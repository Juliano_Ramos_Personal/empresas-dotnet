﻿namespace IoaSysEnterprise.WebApi.Data.Entities
{
    public class Enterprise
    {
        public long Id { get; set; }
        public long EnterpriseType_Id { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
      
        public virtual EnterpriseType EnterpriseType { get; set; }
    }
}
