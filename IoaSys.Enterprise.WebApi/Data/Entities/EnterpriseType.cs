﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IoaSysEnterprise.WebApi.Data.Entities
{
    public class EnterpriseType
    {
        public long Id { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<Enterprise> Enterprise { get; set; }
    }
}
