﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoaSysEnterprise.WebApi.Auth
{
    public class AuthOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = "IoaSysAuthenticationScheme";
        public string Scheme => DefaultScheme;
        public StringValues AuthKey { get; set; }

        public TokenValidationParameters Validations { get; set; }
    }
}
